using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
// remember to add this !!!!!!
using WebAPI2;


namespace ZieglerConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			// The code provided will print ‘Hello World’ to the console.
			// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
			Console.WriteLine("Hello World!");

			const string ServerUrl = "HTTP://localhost:5500";
			HttpClientHandler handler = new HttpClientHandler();
			handler.UseDefaultCredentials = true;

			using (var client = new HttpClient(handler))
			{
				client.BaseAddress = new Uri(ServerUrl);
				client.DefaultRequestHeaders.Clear();

				// get food/raw groups

				// works
				#region Print / get / read

				try
				{
					var response = client.GetAsync(ServerUrl + "/api/Groups").Result;
					if (response.IsSuccessStatusCode)
					{
						// the API made the model, but added an s to it .... x(
						var groups = response.Content.ReadAsAsync<IEnumerable<WebAPI2.Models.Groups>>().Result;
						foreach (var group in groups)
						{
							Console.WriteLine(group);
						}
					}
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					throw;
				}

				#endregion

				// works
				#region Create / add / Post

				//try
				//{
				//	Groups tempGroup = new Groups() {Id = 10, name = "test value"};

				//	// it needs to be "PostAsJsonAsync", since "PostAsync" don't work here
				//	var response = client.PostAsJsonAsync(ServerUrl + "/api/Groups", tempGroup).Result;
				//	if (response.IsSuccessStatusCode)
				//	{
				//		Console.WriteLine("you have now added a temp Group value, to the database");
				//	}
				//}
				//catch (Exception e)
				//{
				//	Console.WriteLine(e);
				//	throw;
				//}

				#endregion

				#region Create / add / Post (without id)

				//try
				//{
				//	FoodReaction tempReaction = new FoodReaction() { Fk_food_id = 5, Fk_type_id = 5, Comment = "big dick", Rating = 55};

				//	// it needs to be "PostAsJsonAsync", since "PostAsync" don't work here
				//	var response = client.PostAsJsonAsync(ServerUrl + "/api/FoodReactions", tempReaction).Result;
				//	if (response.IsSuccessStatusCode)
				//	{
				//		Console.WriteLine("you have now added a temp reaction value, to the database");
				//	}
				//}
				//catch (Exception e)
				//{
				//	Console.WriteLine(e);
				//	throw;
				//}

				#endregion

				// works
				#region Edit / update / put

				//try
				//{
				//	// you can see that, all these have the same names.
				//	// That's never a problem, since they all are inside their own scoop

				//	// you can change the name attribute, but not the ID attribute
				//	Groups tempGroup = new Groups() { Id = 10, name = "test value new" };

				//	var response = client.PutAsJsonAsync(ServerUrl + "/api/Groups/" + 10, tempGroup).Result;
				//	if (response.IsSuccessStatusCode)
				//	{
				//		Console.WriteLine("it should be edited now");
				//	}
				//	else
				//	{
				//		Console.WriteLine("the edit (put) request didn't work.");
				//		Console.WriteLine("error 40 xD, am I right ?");
				//	}
				//}
				//catch (Exception e)
				//{
				//	Console.WriteLine(e);
				//	throw;
				//}

				#endregion

				// works
				#region delete / remove 

				//try
				//{
				//	// this is the one we will delete, but we just need the id, not the object
				//	//Groups tempGroup = new Groups() { Id = 10, name = "test value new" };

				//	var response = client.DeleteAsync(ServerUrl + "/api/Groups/10").Result;
				//	if (response.IsSuccessStatusCode)
				//	{
				//		Console.WriteLine("you have delete temp Group id 10, from the database");
				//	}
				//	else
				//	{
				//		Console.WriteLine("the delete request didn't work.");
				//		Console.WriteLine("error 40 xD, am I right ?");
				//	}
				//}
				//catch (Exception e)
				//{
				//	Console.WriteLine(e);
				//	throw;
				//}

				#endregion

				Console.WriteLine();
				Console.WriteLine("lets just see them all again");
				Console.WriteLine();

				// extra print part
				//try
				//{
				//	var response = client.GetAsync(ServerUrl + "/api/FoodReactions").Result;
				//	if (response.IsSuccessStatusCode)
				//	{
				//		// the API made the model, but added an s to it .... x(
				//		var groups = response.Content.ReadAsAsync<IEnumerable<FoodReaction>>().Result;
				//		foreach (var group in groups)
				//		{
				//			Console.WriteLine(group);
				//		}
				//	}
				//}
				//catch (Exception e)
				//{
				//	Console.WriteLine(e);
				//	throw;
				//}
			}
			

			Console.ReadKey();

			// Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
		}
	}
}
