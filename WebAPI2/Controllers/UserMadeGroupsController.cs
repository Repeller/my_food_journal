﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
    public class UserMadeGroupsController : ApiController
    {
        private ModelFoodContext db = new ModelFoodContext();

        // GET: api/UserMadeGroups
        public IQueryable<UserMadeGroups> GetUserMadeGroups()
        {
            return db.UserMadeGroups;
        }

        // GET: api/UserMadeGroups/5
        [ResponseType(typeof(UserMadeGroups))]
        public IHttpActionResult GetUserMadeGroups(int id)
        {
            UserMadeGroups userMadeGroups = db.UserMadeGroups.Find(id);
            if (userMadeGroups == null)
            {
                return NotFound();
            }

            return Ok(userMadeGroups);
        }

        // PUT: api/UserMadeGroups/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUserMadeGroups(int id, UserMadeGroups userMadeGroups)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userMadeGroups.Id)
            {
                return BadRequest();
            }

            db.Entry(userMadeGroups).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserMadeGroupsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserMadeGroups
        [ResponseType(typeof(UserMadeGroups))]
        public IHttpActionResult PostUserMadeGroups(UserMadeGroups userMadeGroups)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UserMadeGroups.Add(userMadeGroups);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = userMadeGroups.Id }, userMadeGroups);
        }

        // DELETE: api/UserMadeGroups/5
        [ResponseType(typeof(UserMadeGroups))]
        public IHttpActionResult DeleteUserMadeGroups(int id)
        {
            UserMadeGroups userMadeGroups = db.UserMadeGroups.Find(id);
            if (userMadeGroups == null)
            {
                return NotFound();
            }

            db.UserMadeGroups.Remove(userMadeGroups);
            db.SaveChanges();

            return Ok(userMadeGroups);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserMadeGroupsExists(int id)
        {
            return db.UserMadeGroups.Count(e => e.Id == id) > 0;
        }
    }
}