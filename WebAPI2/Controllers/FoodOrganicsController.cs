﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
    public class FoodOrganicsController : ApiController
    {
        private ModelFoodContext db = new ModelFoodContext();

        // GET: api/FoodOrganics
        public IQueryable<FoodOrganic> GetFoodOrganic()
        {
            return db.FoodOrganic;
        }

        // GET: api/FoodOrganics/5
        [ResponseType(typeof(FoodOrganic))]
        public IHttpActionResult GetFoodOrganic(int id)
        {
            FoodOrganic foodOrganic = db.FoodOrganic.Find(id);
            if (foodOrganic == null)
            {
                return NotFound();
            }

            return Ok(foodOrganic);
        }

        // PUT: api/FoodOrganics/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFoodOrganic(int id, FoodOrganic foodOrganic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != foodOrganic.Id)
            {
                return BadRequest();
            }

            db.Entry(foodOrganic).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FoodOrganicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FoodOrganics
        [ResponseType(typeof(FoodOrganic))]
        public IHttpActionResult PostFoodOrganic(FoodOrganic foodOrganic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FoodOrganic.Add(foodOrganic);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = foodOrganic.Id }, foodOrganic);
        }

        // DELETE: api/FoodOrganics/5
        [ResponseType(typeof(FoodOrganic))]
        public IHttpActionResult DeleteFoodOrganic(int id)
        {
            FoodOrganic foodOrganic = db.FoodOrganic.Find(id);
            if (foodOrganic == null)
            {
                return NotFound();
            }

            db.FoodOrganic.Remove(foodOrganic);
            db.SaveChanges();

            return Ok(foodOrganic);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FoodOrganicExists(int id)
        {
            return db.FoodOrganic.Count(e => e.Id == id) > 0;
        }
    }
}