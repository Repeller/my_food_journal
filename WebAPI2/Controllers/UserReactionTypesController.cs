﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
    public class UserReactionTypesController : ApiController
    {
        private ModelFoodContext db = new ModelFoodContext();

        // GET: api/UserReactionTypes
        public IQueryable<UserReactionTypes> GetUserReactionTypes()
        {
            return db.UserReactionTypes;
        }

        // GET: api/UserReactionTypes/5
        [ResponseType(typeof(UserReactionTypes))]
        public IHttpActionResult GetUserReactionTypes(int id)
        {
            UserReactionTypes userReactionTypes = db.UserReactionTypes.Find(id);
            if (userReactionTypes == null)
            {
                return NotFound();
            }

            return Ok(userReactionTypes);
        }

        // PUT: api/UserReactionTypes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUserReactionTypes(int id, UserReactionTypes userReactionTypes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userReactionTypes.Id)
            {
                return BadRequest();
            }

            db.Entry(userReactionTypes).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserReactionTypesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserReactionTypes
        [ResponseType(typeof(UserReactionTypes))]
        public IHttpActionResult PostUserReactionTypes(UserReactionTypes userReactionTypes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UserReactionTypes.Add(userReactionTypes);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = userReactionTypes.Id }, userReactionTypes);
        }

        // DELETE: api/UserReactionTypes/5
        [ResponseType(typeof(UserReactionTypes))]
        public IHttpActionResult DeleteUserReactionTypes(int id)
        {
            UserReactionTypes userReactionTypes = db.UserReactionTypes.Find(id);
            if (userReactionTypes == null)
            {
                return NotFound();
            }

            db.UserReactionTypes.Remove(userReactionTypes);
            db.SaveChanges();

            return Ok(userReactionTypes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserReactionTypesExists(int id)
        {
            return db.UserReactionTypes.Count(e => e.Id == id) > 0;
        }
    }
}