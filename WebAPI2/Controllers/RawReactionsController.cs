﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
    public class RawReactionsController : ApiController
    {
        private ModelFoodContext db = new ModelFoodContext();

        // GET: api/RawReactions
        public IQueryable<RawReaction> GetRawReaction()
        {
            return db.RawReaction;
        }

        // GET: api/RawReactions/5
        [ResponseType(typeof(RawReaction))]
        public IHttpActionResult GetRawReaction(int id)
        {
            RawReaction rawReaction = db.RawReaction.Find(id);
            if (rawReaction == null)
            {
                return NotFound();
            }

            return Ok(rawReaction);
        }

        // PUT: api/RawReactions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRawReaction(int id, RawReaction rawReaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rawReaction.Id)
            {
                return BadRequest();
            }

            db.Entry(rawReaction).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RawReactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RawReactions
        [ResponseType(typeof(RawReaction))]
        public IHttpActionResult PostRawReaction(RawReaction rawReaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RawReaction.Add(rawReaction);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = rawReaction.Id }, rawReaction);
        }

        // DELETE: api/RawReactions/5
        [ResponseType(typeof(RawReaction))]
        public IHttpActionResult DeleteRawReaction(int id)
        {
            RawReaction rawReaction = db.RawReaction.Find(id);
            if (rawReaction == null)
            {
                return NotFound();
            }

            db.RawReaction.Remove(rawReaction);
            db.SaveChanges();

            return Ok(rawReaction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RawReactionExists(int id)
        {
            return db.RawReaction.Count(e => e.Id == id) > 0;
        }
    }
}