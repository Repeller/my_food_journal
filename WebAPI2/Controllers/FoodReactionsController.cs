﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
    public class FoodReactionsController : ApiController
    {
        private ModelFoodContext db = new ModelFoodContext();

        // GET: api/FoodReactions
        public IQueryable<FoodReaction> GetFoodReaction()
        {
            return db.FoodReaction;
        }

        // GET: api/FoodReactions/5
        [ResponseType(typeof(FoodReaction))]
        public IHttpActionResult GetFoodReaction(int id)
        {
            FoodReaction foodReaction = db.FoodReaction.Find(id);
            if (foodReaction == null)
            {
                return NotFound();
            }

            return Ok(foodReaction);
        }

        // PUT: api/FoodReactions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFoodReaction(int id, FoodReaction foodReaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != foodReaction.Id)
            {
                return BadRequest();
            }

            db.Entry(foodReaction).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FoodReactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FoodReactions
        [ResponseType(typeof(FoodReaction))]
        public IHttpActionResult PostFoodReaction(FoodReaction foodReaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FoodReaction.Add(foodReaction);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = foodReaction.Id }, foodReaction);
        }

        // DELETE: api/FoodReactions/5
        [ResponseType(typeof(FoodReaction))]
        public IHttpActionResult DeleteFoodReaction(int id)
        {
            FoodReaction foodReaction = db.FoodReaction.Find(id);
            if (foodReaction == null)
            {
                return NotFound();
            }

            db.FoodReaction.Remove(foodReaction);
            db.SaveChanges();

            return Ok(foodReaction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FoodReactionExists(int id)
        {
            return db.FoodReaction.Count(e => e.Id == id) > 0;
        }
    }
}