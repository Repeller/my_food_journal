﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
    public class RawOrganicsController : ApiController
    {
        private ModelFoodContext db = new ModelFoodContext();

        // GET: api/RawOrganics
        public IQueryable<RawOrganic> GetRawOrganic()
        {
            return db.RawOrganic;
        }

        // GET: api/RawOrganics/5
        [ResponseType(typeof(RawOrganic))]
        public IHttpActionResult GetRawOrganic(int id)
        {
            RawOrganic rawOrganic = db.RawOrganic.Find(id);
            if (rawOrganic == null)
            {
                return NotFound();
            }

            return Ok(rawOrganic);
        }

        // PUT: api/RawOrganics/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRawOrganic(int id, RawOrganic rawOrganic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rawOrganic.Id)
            {
                return BadRequest();
            }

            db.Entry(rawOrganic).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RawOrganicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RawOrganics
        [ResponseType(typeof(RawOrganic))]
        public IHttpActionResult PostRawOrganic(RawOrganic rawOrganic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RawOrganic.Add(rawOrganic);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = rawOrganic.Id }, rawOrganic);
        }

        // DELETE: api/RawOrganics/5
        [ResponseType(typeof(RawOrganic))]
        public IHttpActionResult DeleteRawOrganic(int id)
        {
            RawOrganic rawOrganic = db.RawOrganic.Find(id);
            if (rawOrganic == null)
            {
                return NotFound();
            }

            db.RawOrganic.Remove(rawOrganic);
            db.SaveChanges();

            return Ok(rawOrganic);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RawOrganicExists(int id)
        {
            return db.RawOrganic.Count(e => e.Id == id) > 0;
        }
    }
}