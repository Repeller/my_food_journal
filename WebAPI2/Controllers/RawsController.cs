﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
    public class RawsController : ApiController
    {
        private ModelFoodContext db = new ModelFoodContext();

        // GET: api/Raws
        public IQueryable<Raw> GetRaw()
        {
            return db.Raw;
        }

        // GET: api/Raws/5
        [ResponseType(typeof(Raw))]
        public IHttpActionResult GetRaw(int id)
        {
            Raw raw = db.Raw.Find(id);
            if (raw == null)
            {
                return NotFound();
            }

            return Ok(raw);
        }

        // PUT: api/Raws/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRaw(int id, Raw raw)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != raw.Id)
            {
                return BadRequest();
            }

            db.Entry(raw).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RawExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Raws
        [ResponseType(typeof(Raw))]
        public IHttpActionResult PostRaw(Raw raw)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Raw.Add(raw);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = raw.Id }, raw);
        }

        // DELETE: api/Raws/5
        [ResponseType(typeof(Raw))]
        public IHttpActionResult DeleteRaw(int id)
        {
            Raw raw = db.Raw.Find(id);
            if (raw == null)
            {
                return NotFound();
            }

            db.Raw.Remove(raw);
            db.SaveChanges();

            return Ok(raw);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RawExists(int id)
        {
            return db.Raw.Count(e => e.Id == id) > 0;
        }
    }
}