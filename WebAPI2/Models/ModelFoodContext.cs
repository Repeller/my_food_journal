namespace WebAPI2.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelFoodContext : DbContext // ModelFoodContext er nedarver fra DbContext og er Forbindenelsen til databasen.
    {
        public ModelFoodContext()
            : base("name=ModelFoodContext123")
        {
        }

        public virtual DbSet<Food> Food { get; set; }
        public virtual DbSet<FoodOrganic> FoodOrganic { get; set; }
        public virtual DbSet<FoodReaction> FoodReaction { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<Raw> Raw { get; set; }
        public virtual DbSet<RawOrganic> RawOrganic { get; set; }
        public virtual DbSet<RawReaction> RawReaction { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserMadeGroups> UserMadeGroups { get; set; }
        public virtual DbSet<UserReactionTypes> UserReactionTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
