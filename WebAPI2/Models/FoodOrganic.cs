namespace WebAPI2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FoodOrganic")]
    public partial class FoodOrganic
    {
        public int Id { get; set; }

        public int fk_food_id { get; set; }
    }
}
