namespace WebAPI2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RawOrganic")]
    public partial class RawOrganic
    {
        public int Id { get; set; }

        public int fk_raw_id { get; set; }
    }
}
