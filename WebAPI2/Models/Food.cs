namespace WebAPI2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Food")]
    public partial class Food
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string BoughtFrom { get; set; }

        [Required]
        [StringLength(50)]
        public string MadeByCompany { get; set; }

        public int fk_groups_id { get; set; }

        public int fk_user_id { get; set; }
    }
}
