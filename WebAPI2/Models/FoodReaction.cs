namespace WebAPI2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FoodReaction")]
    public partial class FoodReaction
    {
        public int Id { get; set; }

        public int fk_type_id { get; set; }

        public int Rating { get; set; }

        [Required]
        [StringLength(300)]
        public string Comment { get; set; }

        public int fk_food_id { get; set; }
    }
}
