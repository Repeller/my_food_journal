namespace WebAPI2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Raw")]
    public partial class Raw
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string BoughtFrom { get; set; }

        [Required]
        [StringLength(50)]
        public string MadeByCompany { get; set; }

        public int fk_grops_id { get; set; }

        public int fk_user_id { get; set; }
    }
}
