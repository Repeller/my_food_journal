using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WebAPI2.Models;

namespace AnjasConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            const string ServerUrl = "http://localhost:5500";

            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync($"{ServerUrl}/api/Users/").Result;
                if (response.IsSuccessStatusCode)
                {
                    List<User> users = response.Content.ReadAsAsync<List<User>>().Result;

                    foreach (var user in users)
                    {
                        Console.WriteLine($"Username: {user.Username}");
                    }

                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("Something went wrong");
                }
            }
        }
    }
}
