﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyFoodApp.Model;

namespace MyFoodApp.Persistency
{
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
	class GroupPersistencyService
	{
        //public const string serverUrl = "http://localhost:5500";

        /// <summary>
        /// read (get) all the groups items from the database
        /// </summary>
        /// <returns>A list of Group objects</returns>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static async Task<List<Group>> GetGroupsAsync()
		{
			const string serverUrl = "http://localhost:5500";

			HttpClientHandler handler = new HttpClientHandler();
			handler.UseDefaultCredentials = true;

			using (var client = new HttpClient(handler))
			{
				client.BaseAddress = new Uri(serverUrl);
				client.DefaultRequestHeaders.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

				var response =  client.GetAsync(serverUrl + "/api/Groups").Result;
				if (response.IsSuccessStatusCode)
				{
					IEnumerable<Group> groups = await response.Content.ReadAsAsync<IEnumerable<Group>>();
					return groups.ToList();
				}
				else
				{
					//Debug.WriteLine("could not read from database");
				}
			}

			// TODO : this needs to be better
			return null;
		}

        /// <summary>
        /// add (post) one Group item to the database
        /// </summary>
        /// <param name="item">the new group item</param>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static async void PostGroupAsync(Group item)
		{
			const string serverUrl = "http://localhost:5500";

			HttpClientHandler handler = new HttpClientHandler();
			handler.UseDefaultCredentials = true;

			using (var client = new HttpClient(handler))
			{
				client.BaseAddress = new Uri(serverUrl);
				client.DefaultRequestHeaders.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

				var response = await client.PostAsJsonAsync(serverUrl + "/Api/Groups", item);

				// TODO : give some kind of live feedback, to know if it works.

				// TODO : comment out the live feedback, before you release this
			}
		}

        /// <summary>
        /// edit (put) one group value in the database
        /// </summary>
        /// <param name="item">needs the id of the group value you want to edit (warning, you can only edit "name")</param>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static async void PutGroupAsync(Group item)
		{
			const string serverUrl = "http://localhost:5500";

			HttpClientHandler handler = new HttpClientHandler();
			handler.UseDefaultCredentials = true;

			using (var client = new HttpClient(handler))
			{
				client.BaseAddress = new Uri(serverUrl);
				client.DefaultRequestHeaders.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

				var response = await client.PutAsJsonAsync(serverUrl + "/Api/Groups/" + item.Id, item);

				// TODO : give some kind of live feedback, to know if it works.

				// TODO : comment out the live feedback, before you release this
			}
		}

        /// <summary>
        /// delete a group value from the database
        /// </summary>
        /// <param name="item">you only need the id, of the item you want to delete</param>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static async void DeleteGroupAsync(Group item)
		{
			const string serverUrl = "http://localhost:5500";

			HttpClientHandler handler = new HttpClientHandler();
			handler.UseDefaultCredentials = true;

			using (var client = new HttpClient(handler))
			{
				client.BaseAddress = new Uri(serverUrl);
				client.DefaultRequestHeaders.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

				var response = await client.DeleteAsync(serverUrl + "/Api/Groups/" + item.Id);

				// TODO : give some kind of live feedback, to know if it works.

				// TODO : comment out the live feedback, before you release this
			}
		}
	}
}
