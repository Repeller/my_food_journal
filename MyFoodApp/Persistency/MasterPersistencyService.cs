﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using MyFoodApp.Model;

namespace MyFoodApp.Persistency
{
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
    public class MasterPersistencyService<T>
    {
	    private const string serverUrl = "http://localhost:5500";

        /// <summary>
        /// the value that change, to which every controller you picked
        /// </summary>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        private static string extraUrl;

        /// <summary>
        /// all the kind of classes that you can use in this Persistency class
        /// </summary>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public enum DataControllerType
		{
			Food, FoodOrganic, FoodReaction, 
			Groups, UserMadeGroups, 
			Raw, RawReaction, RawOrganic,
			User, UserReactionTypes
		}

	    public MasterPersistencyService()
	    {
		}

        /// <summary>
        /// Change the 'extraUrl' string. Gets called every time you use a HTTP method.
        /// </summary>
        /// <param name="type">the class type that you are using</param>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static void SetType(DataControllerType type)
	    {
		    switch (type)
		    {
				case MasterPersistencyService<T>.DataControllerType.Food:
					extraUrl = "/api/Foods";
					break;
				case MasterPersistencyService<T>.DataControllerType.FoodOrganic:
					extraUrl = "/api/FoodOrganics";
					break;
				case MasterPersistencyService<T>.DataControllerType.FoodReaction:
					extraUrl = "/api/FoodReactions";
					break;
				case MasterPersistencyService<T>.DataControllerType.Groups:
					extraUrl = "/api/Groups";
					break;
				case MasterPersistencyService<T>.DataControllerType.UserMadeGroups:
					extraUrl = "/api/UserMadeGroups";
					break;
				case MasterPersistencyService<T>.DataControllerType.Raw:
					extraUrl = "/api/Raws";
					break;
				case MasterPersistencyService<T>.DataControllerType.RawReaction:
					extraUrl = "/api/RawReactions";
					break;
				case MasterPersistencyService<T>.DataControllerType.RawOrganic:
					extraUrl = "/api/RawOrganics";
					break;
				case MasterPersistencyService<T>.DataControllerType.UserReactionTypes:
					extraUrl = "/api/UserReactionTypes";
					break;
				case MasterPersistencyService<T>.DataControllerType.User:
					extraUrl = "/api/Users";
					break;
			}
	    }

        /// <summary>
        /// Update one entity in your database
        /// </summary>
        /// <param name="type">the type of class you are using</param>
        /// <param name="id">the id of the entity that you want to update</param>
        /// <param name="item">the item with the new values</param>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static async void UpdateAsync(DataControllerType type, int id, T item)
	    {

			HttpClientHandler handler = new HttpClientHandler();
			handler.UseDefaultCredentials = true;

			HttpClient client = new HttpClient(handler);
			client.BaseAddress = new Uri(serverUrl);

			client.DefaultRequestHeaders.Clear();
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			// id 
			SetType(type);

		    using (client)
		    {
				var response = await client.PutAsJsonAsync(serverUrl + extraUrl + "/" + id, item);
			}
	    }

        /// <summary>
        /// add one entity to the database
        /// </summary>
        /// <param name="type">the type of class you are using</param>
        /// <param name="item">the entity you want to add</param>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static async void SaveAsync(DataControllerType type, T item)
	    {
			SetType(type);

			HttpClientHandler handler = new HttpClientHandler();
			handler.UseDefaultCredentials = true;

			HttpClient client = new HttpClient(handler);
			client.BaseAddress = new Uri(serverUrl);

			client.DefaultRequestHeaders.Clear();
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			using (client)
			{
				var response = await client.PostAsJsonAsync(serverUrl + extraUrl, item);
			}
	    }

        /// <summary>
        /// Delete one entity from the database
        /// </summary>
        /// <param name="type">the type of class you are using</param>
        /// <param name="id">the id of the entity that you want to delete</param>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static async void DeleteAsync(DataControllerType type, int id)
	    {
			// id
		    SetType(type);

		    HttpClientHandler handler = new HttpClientHandler();
		    handler.UseDefaultCredentials = true;

		    HttpClient client = new HttpClient(handler);
		    client.BaseAddress = new Uri(serverUrl);

		    client.DefaultRequestHeaders.Clear();
		    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			using (client)
		    {
			    var response = await client.DeleteAsync((serverUrl + extraUrl + "/" + id) );
		    }
		}

        /// <summary>
        /// Gets all the entities from one database table
        /// </summary>
        /// <param name="type">the type of class or table you want</param>
        /// <returns>A list of entities, that has the same type as 'Type'</returns>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static async Task<List<T>> LoadAsync(DataControllerType type)
	    {
			// dont need to be edited
		    SetType(type);

		    HttpClientHandler handler = new HttpClientHandler();
		    handler.UseDefaultCredentials = true;

		    HttpClient client = new HttpClient(handler);
		    client.BaseAddress = new Uri(serverUrl);

		    client.DefaultRequestHeaders.Clear();
		    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			using (client)
			{
				var response =  client.GetAsync(serverUrl + extraUrl).Result;
				if (response.IsSuccessStatusCode)
				{
					IEnumerable<T> Items = await response.Content.ReadAsAsync<IEnumerable<T>>();
					return Items.ToList();
				}
				else
				{
					// give feedback
				}
			}

			return null;
	    }
	}
}
