﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyFoodApp.Model;

namespace MyFoodApp.Persistency
{
    class RawPersistencyService  // 
    {
        /*
        Persistency er den eneste klasse som snakker med DataBasen.
         */
         
         //som henter alle min Rawitems som jeg har tilføjet i min Database.
        public static async Task<List<RawItem>> GetRawItemsAsync()
        {
            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response1 = client.GetAsync("http://localhost:5500/api/Raws").Result;
                if (response1.IsSuccessStatusCode)
                {
                    IEnumerable<RawItem> Rawlist = response1.Content.ReadAsAsync<IEnumerable<RawItem>>().Result;
                    return (List<RawItem>)Rawlist;
                }
            }
            return null;
        }

        // For at tilføje til listen.
        public static async void PostRawItemAsync(RawItem rawitem)
        {
            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var post = await client.PostAsJsonAsync("http://localhost:5500/Api/Raws", rawitem);
                if(post.IsSuccessStatusCode)
                { }
                else
                {
                    
                }
            }
        }

        // For at Edit i listen.
        public static async void PutRawItemAsync(RawItem rawItem)
        {
            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var put = await client.PutAsJsonAsync("Api/Raws/", rawItem);
            }
        }

        //For at delete i listen
        public static async void DeleteRawItemAsync(RawItem rawItem)
        {
            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var delete = await client.DeleteAsync("Api/Raws/" + rawItem.Id);
            }
        }
    }
}
