﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyFoodApp.Model;

namespace MyFoodApp.Persistency
{
    class ReactionPersistencyService //Indeholder HTTP Request metoder(Get, Put, Post, Delete)
    {
        //Henter alle reactions fra databasen
        //og returnere en liste af reactions
        public static List<Reactions> GetReactions()
        {
            const string serverUrl = "http://localhost:5500";

            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(serverUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync("http://localhost:5500/api/FoodReactions").Result;
                if (response.IsSuccessStatusCode)
                {
                    IEnumerable<Reactions> ReactionList = response.Content.ReadAsAsync<IEnumerable<Reactions>>().Result;
                    return ReactionList.ToList();
                }
                return null;
            }
        }
        /// <summary>
        /// Tilføjer et reactions objekt til databasen
        /// </summary>
        /// <param name="reactions">Tager et objekt som parameter af typen Reactions</param>
        public static async void PostReactionAsync(Reactions reactions)
        {
            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var post = await client.PostAsJsonAsync("http://localhost:5500/api/FoodReactions/", reactions);
            }
        }
        /// <summary>
        /// Redigere et element i databasen ud fra et valgt ID
        /// </summary>
        /// <param name="reactions">Tager et objekt som parameter af typen Reactions</param>
        /// TODO : skal omformuleres
        public static async void PutGroupAsync(Reactions reactions)
        {
            const string serverUrl = "http://localhost:5500";

            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(serverUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.PutAsJsonAsync("http://localhost:5500/" + reactions.Id, reactions);
            }
        }
        /// <summary>
        /// Fjerner et reactions element fra databasen ud fra et ID
        /// </summary>
        /// <param name="reactions">Tager er objekt af typen Reactions som parameter</param>
        public static async void DeleteGroupAsync(Reactions reactions)
        {
            const string serverUrl = "http://localhost:5500";

            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;
            
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(serverUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.DeleteAsync("http://localhost:5500/api/FoodReactions/" + reactions.Id);
            }
        }
    }
}