﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyFoodApp.Model;

namespace MyFoodApp.Persistency
{
    class UserPersistencyService
    {
        public static async Task<User> GetUserAsync(string username)
        {
            const string ServerUrl = "http://localhost:5500";

            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync($"{ServerUrl}/api/Users/");

                if (!response.IsSuccessStatusCode)
                    return null;

                List<User> users = await response.Content.ReadAsAsync<List<User>>();

                User user = users.FirstOrDefault(x => string.Equals(x.Username, username, StringComparison.CurrentCultureIgnoreCase));
                return user;
            }
        }

        public static async void PostUserAsync(User user)
        {
            const string ServerUrl = "http://localhost:5500";

            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var post = await client.PostAsJsonAsync($"{ServerUrl}/api/Users/", user);
            }
        }

        public static async void PutUserAsync(User user)
        {
            const string ServerUrl = "http://localhost:5500";

            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var put = await client.PutAsJsonAsync($"{ServerUrl}/api/Users/{user.Id}", user);
            }
        }

        public static async void DeleteUserAsync(User user)
        {
            const string ServerUrl = "http://localhost:5500";

            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                 var delete = await client.DeleteAsync($"{ServerUrl}/api/Users/{user.Id}");
            }
        }
    }
}
