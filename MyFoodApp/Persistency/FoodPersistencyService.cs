﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyFoodApp.Model;

namespace MyFoodApp.Persistency
{
    class FoodPersistencyService  // Indeholder HTTP Request metoder(Get, Put, Post, Delete)
    {

        //Henter alle FoodItems fra databasen
        //og returnere en liste af FoodItems
        public static async Task<List<FoodItem>> GetFoodItems()
        {
            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response1 = client.GetAsync("http://localhost:5500/api/Foods/").Result;
                if (response1.IsSuccessStatusCode)
                {
                    IEnumerable<FoodItem> foodList = await response1.Content.ReadAsAsync<IEnumerable<FoodItem>>();
                    return foodList.ToList();
                }
            }
            return null;
        }

       
        /// <summary>
        /// Tilføjer et fooditem objekt til databasen
        /// </summary>
        /// <param name="foodItem">Tager et objekt som parameter af typen FoodItem</param>
        public static async void PostFoodItemAsync(FoodItem foodItem)
        {
            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var post = await client.PostAsJsonAsync("http://localhost:5500/api/Foods/", foodItem);
            }
        }

        

        /// <summary>
        /// Laver et request for at kunne Delete et element fra databasen udfra et ID
        /// </summary>
        /// <param name="foodItem"></param>
        public static async void DeleteFoodItemAsync(FoodItem foodItem)
        {
            const string ServerUrl = "http://localhost:5500";

            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                 var delete = await client.DeleteAsync("http://localhost:5500/api/Foods/" + foodItem.Id );
              
            }

        }
    }
}
