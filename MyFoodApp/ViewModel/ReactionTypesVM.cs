﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFoodApp.Model;
using MyFoodApp.Common;
using MyFoodApp.Persistency;

namespace MyFoodApp.ViewModel
{
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
	class ReactionTypesVM : INotifyPropertyChanged
	{

		#region PropertyChangeSupport
		public event PropertyChangedEventHandler PropertyChanged;

		// Create the OnPropertyChanged method to raise the event
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
		#endregion

		#region Fields
		private int _selectedId;
		private string _feedBack;

		private string _newName;
		private string _editNewName;
		
		private string _editVisible;
		private string _addVisible;

		private ReactionType _reactionTypeToBeEdited;
		#endregion

		#region Props
		public int SelectedIndex
		{
			get
			{
				return _selectedId;
			}
			set
			{
				_selectedId = value;
				OnPropertyChanged(nameof(SelectedIndex));
			}
		}

		public string EditVisible
		{
			get { return _editVisible; }
			set
			{
				_editVisible = value;
				OnPropertyChanged(nameof(EditVisible));
			}
		}

		public string AddVisible
		{
			get
			{
				return _addVisible;
			}
			set
			{
				_addVisible = value;
				OnPropertyChanged(nameof(AddVisible));
			}
		}

		public ObservableCollection<ReactionType> ReactionTypes { get; set; }

		public string NewName
		{
			get { return _newName; }
			set
			{
				_newName = value;
				OnPropertyChanged(nameof(NewName));
			}
		}
		public string Feedback
		{
			get { return _feedBack; }
			set
			{
				_feedBack = value;
				OnPropertyChanged(nameof(Feedback));
			}
		}
		public string EditNewName
		{
			get { return _editNewName; }
			set
			{
				_editNewName = value;
				OnPropertyChanged(nameof(EditNewName));
			}
		}
		#endregion

		#region RelayCommands
		public Relaycommand DeleteCommand { get; set; }
		public Relaycommand UpdateTheList { get; set; }

		public Relaycommand EditCommand { get; set; }
		public Relaycommand StartEditCommand { get; set; }
		public Relaycommand StopEditCommand { get; set; }

		public Relaycommand StartAddCommand { get; set; }
		public Relaycommand AddCommand { get; set; }
		public Relaycommand StopAddCommand { get; set; } 
		#endregion

		public ReactionTypesVM()
		{
			ReactionTypes = new ObservableCollection<ReactionType>();
			_reactionTypeToBeEdited = new ReactionType(0, "", 0);

			
			DeleteCommand = new Relaycommand(DeleteOne);
			UpdateTheList = new Relaycommand(UpdateListView);

			AddCommand = new Relaycommand(AddOne);
			StartAddCommand = new Relaycommand(StartAdd);
			StopAddCommand = new Relaycommand(StopAdd);

			StartEditCommand = new Relaycommand(StartEdit);
			EditCommand = new Relaycommand(EditOne);
			StopEditCommand = new Relaycommand(StopEdit);

			GetData();

			AddVisible = "Collapsed";
			EditVisible = "Collapsed";
		}

		/// <summary>
		/// resets all the value that was changed. Only used on Stop add and Edit
		/// </summary>
		/// <param name="IsItAdd">true is 'add', false is 'edit'</param>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void ResetProps(bool isItAdd)
		{
			if (isItAdd)
			{
				NewName = "";
				AddVisible = "Collapsed";
			}
			else
			{
				EditNewName = "";
				_reactionTypeToBeEdited = new ReactionType(0, "", 0);
				EditVisible = "Collapsed";
			}

			SelectedIndex = -1;
			Feedback = "";

		}

		/// <summary>
		/// clean the list and reload the data from the database
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void UpdateListView()
		{
			ReactionTypes.Clear();
			GetData();
		}

		/// <summary>
		/// opens the panel 'Edit' and input data in the textboxes
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void StartEdit()
		{
			if (SelectedIndex != -1 && ReactionTypes.Count != 0)
			{
				_reactionTypeToBeEdited = ReactionTypes[SelectedIndex];
				EditNewName = _reactionTypeToBeEdited.Name; 
				EditVisible = "Visible";
			}
			else
			{
				Feedback = "du skal vælge noget først, som skal edits";
			}
		}
		/// <summary>
		/// opens the panel 'Add'
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void StartAdd()
		{
			AddVisible = "Visible";
		}

		/// <summary>
		/// close the panel 'Edit' and clean the data
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void StopEdit()
		{
			ResetProps(false);
		}
		/// <summary>
		/// close the panel 'Add' and clean the data
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void StopAdd()
		{
			ResetProps(true);
		}

		/// <summary>
		/// delete the one entity in the database table and from the list
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void DeleteOne()
		{
			if (SelectedIndex != -1  && ReactionTypes.Count != 0)
			{
				ReactionType tempType = ReactionTypes[SelectedIndex];
				MasterPersistencyService<ReactionType>.DeleteAsync(
					MasterPersistencyService<ReactionType>.DataControllerType.UserReactionTypes,
					tempType.Id);
				Feedback = " '" + tempType.Name + "' er nu blevet slettet !";
				ReactionTypes.RemoveAt(SelectedIndex);
			}
			else
			{
				Feedback = "vælg noget for at slet det";
			}
		}
		/// <summary>
		/// gets the data from the database, insert it into the ListView
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void GetData()
		{
			// reads the database and adds to the OB list
			List<ReactionType> tempReactionTypes =
				MasterPersistencyService<ReactionType>.LoadAsync(
					MasterPersistencyService<ReactionType>.DataControllerType.UserReactionTypes).Result;
			foreach (ReactionType type in tempReactionTypes)
			{
				ReactionTypes.Add(type);
			}
		}
		/// <summary>
		/// Add one ReactionType to the database, and add it to the list
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void AddOne()
		{
			// 1 validation
			if (NewName != "")
			{
				// TODO: get the user id from somewhere

				// 2 add it to the database
				ReactionType tempType = new ReactionType(0, NewName, 2);
				MasterPersistencyService<ReactionType>.SaveAsync(
					MasterPersistencyService<ReactionType>.DataControllerType.UserReactionTypes, tempType);

				// 3 get the id from the database , get the newest added in the table
				// TODO : this is just a temp work around
				List<ReactionType> tempList =
					MasterPersistencyService<ReactionType>.LoadAsync(
						MasterPersistencyService<ReactionType>.DataControllerType.UserReactionTypes).Result;

			    if (tempList.Any())
			    {
				    tempType = tempList[(tempList.Count - 1)];

			        // 4 add it to the list
			        ReactionTypes.Add(tempType);

			        Feedback = "typen er nu blevet oprettet";
                }
			    else
			    {
			        Feedback = "Der er sket en (måske async) fejl. Klik på updateList knappen";
			    }
                
			}
			else
			{
				Feedback = "der er ikke noget i textboksen";
			}

		}
		/// <summary>
		/// Edit one ReactionType and save it to the database. Update the list item
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void EditOne()
		{
			if (_reactionTypeToBeEdited.Name != EditNewName)
			{
				_reactionTypeToBeEdited.Name = EditNewName;
				MasterPersistencyService<ReactionType>.UpdateAsync(
					MasterPersistencyService<ReactionType>.DataControllerType.UserReactionTypes,
					_reactionTypeToBeEdited.Id, _reactionTypeToBeEdited);
				Feedback = "du har nu gemt din ændring";

				SelectedIndex = -1;
				EditVisible = "Collapsed";
			}
			else
			{
				Feedback = "du har ikke givet et nyt navn. skrevet noget andet";
			}
		}

	}
}
