﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFoodApp.Common;
using MyFoodApp.Model;
using MyFoodApp.Persistency;

namespace MyFoodApp.ViewModel
{
    /// <remarks>
    /// skrevet af Ziegler
    /// </remarks>
	class UserMadeGroupsVM : INotifyPropertyChanged
	{
		#region PropertyChangeSupport

		public event PropertyChangedEventHandler PropertyChanged;

		// Create the OnPropertyChanged method to raise the event
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}

		#endregion

		#region fields

		private string _groupName;
		private string _feedback;

		private int _selectedGroupIndex;

		private string _editGroupName;
		private int _editGroupId;

		private string _addView;
		private string _editView;

		private UserMadeGroup _MadeGroupToBeEdited;

		#endregion

		#region props

		public string GroupName
		{
			get { return _groupName; }
			set
			{
				_groupName = value;
				OnPropertyChanged(nameof(GroupName));
			}
		}

		public string Feedback
		{
			get { return _feedback; }
			set
			{
				_feedback = value;
				OnPropertyChanged(nameof(Feedback));
			}
		}

		public int EditGroupId
		{
			get { return _editGroupId; }
			set
			{
				_editGroupId = value;
				OnPropertyChanged(nameof(EditGroupId));
			}
		}

		public string EditGroupName
		{
			get { return _editGroupName; }
			set
			{
				_editGroupName = value;
				OnPropertyChanged(nameof(EditGroupName));
			}
		}

		public string AddView
		{
			get { return _addView;}
			set
			{
				_addView = value;
				OnPropertyChanged(nameof(AddView));
			}
		}

		public string EditView
		{
			get { return _editView; }
			set
			{
				_editView = value;
				OnPropertyChanged(nameof(EditView));
			}
		}

		public UserMadeGroup MadeGroupToBeEdited
		{
			get { return _MadeGroupToBeEdited;}
			set
			{
				_MadeGroupToBeEdited = value;
				OnPropertyChanged(nameof(MadeGroupToBeEdited));
			}
		}

		public int SelectedGroupIndex
		{
			get { return _selectedGroupIndex; }
			set
			{
				_selectedGroupIndex = value;
				OnPropertyChanged(nameof(SelectedGroupIndex));
			}
		}

		public ObservableCollection<UserMadeGroup> Groups { get; set; }

		#endregion

		#region RelayCommands

		public Relaycommand CommandAdd { get; set; }
		public Relaycommand CommandStopAdd { get; set; }
		public Relaycommand CommandStartAdd { get; set; }

		public Relaycommand CommandDelete { get; set; }
		public Relaycommand CommandUpdateList { get; set; }

		public Relaycommand CommandStartEdit { get; set; }
		public Relaycommand CommandEditGroup { get; set; }
		public Relaycommand CommandStopEdit { get; set; }

		#endregion

		public UserMadeGroupsVM()
		{
			CommandAdd = new Relaycommand(CreateGroup);
			CommandStartAdd = new Relaycommand(StartAddGroup);
			CommandStopAdd = new Relaycommand(StopAddGroup);

			CommandUpdateList = new Relaycommand(GetData);
			CommandDelete = new Relaycommand(DeleteGroup);

			CommandStopEdit = new Relaycommand(EditStopGroup);
			CommandStartEdit = new Relaycommand(EditStartView);
			CommandEditGroup = new Relaycommand(EditGroup);

			// TODO : remove this placeholder test
			Groups = new ObservableCollection<UserMadeGroup>();

			// TODO : get data from the database table
			GetData();

			EditView = "Collapsed";
			AddView = "Collapsed";

			// TODO : get date from the user made data table (but only made by this user)
		}

		/// <summary>
		/// open the edit panel and get the data in the textboxes
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void EditStartView()
		{
			if (SelectedGroupIndex != -1  && Groups.Count != 0)
			{
				MadeGroupToBeEdited = Groups[SelectedGroupIndex];
				EditGroupId = MadeGroupToBeEdited.Id;
				EditGroupName = MadeGroupToBeEdited.Name;
				Feedback = "";
				EditView = "Visible";
			}
			else
			{
				Feedback = "du skal vælge noget";
			}
		}
		/// <summary>
		/// save the new edited value in the database and update the one item in the OC list
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void EditGroup()
		{
			// check if the values are new
			// if no, then do the edit
			if (Groups[SelectedGroupIndex].Name != EditGroupName)
			{
				// TODO : get the fk_user_id from somewhere

				UserMadeGroup OldGroup = MadeGroupToBeEdited;

				MadeGroupToBeEdited.Name = EditGroupName;

				MasterPersistencyService<UserMadeGroup>.UpdateAsync(
					MasterPersistencyService<UserMadeGroup>.DataControllerType.UserMadeGroups,
					MadeGroupToBeEdited.Id, MadeGroupToBeEdited);

				
				Groups[Groups.IndexOf(OldGroup)] = MadeGroupToBeEdited;

				ResetProps(false);

				Feedback = "the edit is now been saved";
			}
			else
			{
				Feedback = "the group name is the same. hvad laver du ?";
			}

		}
		/// <summary>
		/// close the edit panel and call 'retsetprops'
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void EditStopGroup()
		{
			ResetProps(false);
		}
		/// <summary>
		/// delete the one group from the database and the OC list
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void DeleteGroup()
		{

			if (SelectedGroupIndex != -1  && Groups.Count != 0)
			{
				try
				{
					MasterPersistencyService<UserMadeGroup>.DeleteAsync(
						MasterPersistencyService<UserMadeGroup>.DataControllerType.UserMadeGroups,
						Groups[SelectedGroupIndex].Id);
					Groups.RemoveAt(SelectedGroupIndex);
					SelectedGroupIndex = -1;
					Feedback = "the group have now been removed";
				}
				catch (Exception e)
				{
					//System.Console.WriteLine(e);
					throw;
				}
			}
			else
			{
				Feedback = "you have not selected an group";
			}

			//GetData();
		}
		/// <summary>
		/// reset the props from the 'add' or 'edit'
		/// </summary>
		/// <param name="isItAdd">true is 'add', false is 'edit'</param>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void ResetProps(bool isItAdd)
		{
			if (isItAdd)
			{
				AddView = "Collapsed";
				GroupName = "";
			}
			else
			{
				EditView = "Collapsed";
				MadeGroupToBeEdited = new UserMadeGroup(0, "", 0);
				EditGroupName = "";
				EditGroupId = 0;
			}

			SelectedGroupIndex = -1;
			Feedback = "";
		}
		/// <summary>
		/// save the new group in the database and save it to the OC list
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void CreateGroup()
		{
            // add it to the database
			UserMadeGroup tempGroup = new UserMadeGroup(0, GroupName, 2);
			MasterPersistencyService<UserMadeGroup>.SaveAsync(
				MasterPersistencyService<UserMadeGroup>.DataControllerType.UserMadeGroups,
				tempGroup);

            // TODO : this is just a temp work around
            // get the newest id from the database
            List<UserMadeGroup> tempList = MasterPersistencyService<UserMadeGroup>.LoadAsync(
                MasterPersistencyService<UserMadeGroup>.DataControllerType.UserMadeGroups).Result;
            
            // find the id and added to "tempGroup"
		    if (tempList.Any())
		    {
		        tempGroup.Id = tempList[(tempList.Count - 1)].Id;
		        // add it to the OC list
		        Groups.Add(tempGroup);

		        ResetProps(true);
		        Feedback = "Den nye mad gruppe er nu oprettet";
            }
		    else
		    {
		        ResetProps(true);
		        Feedback = "Der skete en (måske async) fejl. Tryk på update listen knappen";
            }
            
		}
		/// <summary>
		/// close the add panel and call 'resetProps'
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void StopAddGroup()
		{
			ResetProps(true);
		}
		/// <summary>
		/// open the add panel
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void StartAddGroup()
		{
			AddView = "Visible";
		}

		//TODO : this method "GetData" needs to be reworked. Could be done by the singleTon
		/// <summary>
		/// get all the groups from the database and put in a OC list
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		private void GetData()
		{
			List<UserMadeGroup> tempGroups =
				MasterPersistencyService<UserMadeGroup>.LoadAsync(
						MasterPersistencyService<UserMadeGroup>.DataControllerType.UserMadeGroups)
					.Result;
			foreach (UserMadeGroup group in tempGroups)
			{
				Groups.Add(group);
			}
		}
	}
}