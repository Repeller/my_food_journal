﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyFoodApp.Common;
using MyFoodApp.Model;
using MyFoodApp.Persistency;
using MyFoodApp.View;

namespace MyFoodApp.ViewModel
{
    class SignUpVM : INotifyPropertyChanged
    {
        private const int MinimumUsernameLength = 4;
        private const int MinimumPasswordLength = 8;

        #region PropertyChangeSupport
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion

        public Relaycommand SignUpCommand { get; set; }

        public string Username { get; set; }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        private string _password2;
        public string Password2
        {
            get { return _password2; }
            set
            {
                _password2 = value;
                OnPropertyChanged(nameof(Password2));
            }
        }

        private string _errorMessage;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        public SignUpVM()
        {
            SignUpCommand = new Relaycommand(SignUpUser);
            Username = string.Empty;
            Password = string.Empty;
            ErrorMessage = string.Empty;
        }

        public async void SignUpUser()
        {
            // 1. Tjek at bruger ikke findes i forvejen
            // 2. Tjek at brugernavn opfylder minimumskrav
            // 3. Tjek at kodeord er ens
            // 4. Tjek at kodeord opfylder minimumskrav
            // 5. Opret ny bruger
            // 6. Hent oprettet bruger fra databasen
            // 7. Oprettet bruger logges ind
            // 8. Redirect til profilside

            User user = await UserPersistencyService.GetUserAsync(Username);

            if (user != null)
            {
                ErrorMessage = "Bruger findes allerede";
            }

            else if (Username.Length < MinimumUsernameLength)
            {
                ErrorMessage = $"Brugernavn er for kort. Benyt mindst {MinimumUsernameLength} tegn";
            }

            else if (Password != Password2)
            {
                ErrorMessage = "Kodeord skal være ens";
            }

            else if (Password.Length < MinimumPasswordLength)
            {
                ErrorMessage = $"Password er for kort. Benyt mindst {MinimumPasswordLength} tegn";
            }

            else
            {
                // Create new user and store in DB
                User newUser = new User(0, Username, Password);
                UserPersistencyService.PostUserAsync(newUser);

                // Get created user from DB
                User createdUser = await UserPersistencyService.GetUserAsync(Username);

                // Persist user in session
                LoggedInUserSingleton.Instance.LoggedInUser = createdUser;

                // Go to main page
                Frame rootFrame = Window.Current.Content as Frame;
                rootFrame.Navigate(typeof(FrontPage));
            }

            Password = string.Empty;
            Password2 = string.Empty;
        }
    }
}
