﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using MyFoodApp.Common;
using MyFoodApp.Persistency;

namespace MyFoodApp.ViewModel
{
    class FoodVM : INotifyPropertyChanged
    {

        #region PropertyChangedSupport

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion


        #region fields
        /// <summary>
        /// Instans felter
        /// </summary>

        private int _foodId;
        private string _foodName;
        private string _feedback;
        private int _selectedFoodIndex;
        private int _selectedEditFoodIndex;


        private int _addfkGroupTypeId;
        private string _addName;
        private string _addMadeByCompany;                                 
        private string _addBoughtFrom;


        
        private string _editFoodName;
        private string _editBoughtFrom;
        private string _editMadeByCompany;
        private int _editGroupTypeId;





        #endregion

        #region props

        public int FoodId
        {
            get { return _foodId; }
            set
            {
                _foodId = value;
                OnPropertyChanged(nameof(FoodId));
            }
        }

        public string FoodName
        {
            get { return _foodName; }
            set
            {
                _foodName = value;
                OnPropertyChanged(nameof(FoodName));
            }
        }

        public string FeedBack
        {
            get { return _feedback; }
            set
            {
                _feedback = value;
                OnPropertyChanged(nameof(FeedBack));
            }
        }


        public string AddFoodName
        {
            get { return _addName; }
            set { _addName = value; OnPropertyChanged(nameof(AddFoodName)); }
        }
        public string AddBoughtFrom
        {
            get { return _addBoughtFrom; }
            set { _addBoughtFrom = value; OnPropertyChanged(nameof(AddBoughtFrom)); }
        }
        public string AddMadeByCompany
        {
            get { return _addMadeByCompany; }
            set { _addMadeByCompany = value; OnPropertyChanged(nameof(AddMadeByCompany)); }
        }
        public int AddFkGroupTypeId
        {
            get { return _addfkGroupTypeId; }
            set { _addfkGroupTypeId = value; OnPropertyChanged(nameof(AddFkGroupTypeId)); }
        }


        public string EditFoodName
        {
            get { return _editFoodName; }
            set { _editFoodName = value; OnPropertyChanged(nameof(EditFoodName)); }
        }

        public string EditBoughFrom
        {
            get { return _editBoughtFrom; }
            set { _editBoughtFrom = value; OnPropertyChanged(nameof(EditBoughFrom)); }
        }

        public string EditMadeByCompany
        {
            get { return _editMadeByCompany; }
            set { _editMadeByCompany = value; OnPropertyChanged(nameof(EditMadeByCompany)); }
        }


        public int EditFkGroupTypeId
        {
            get { return _editGroupTypeId; }
            set { _editGroupTypeId = value; OnPropertyChanged(nameof(EditFkGroupTypeId)); }
        }

        public int SelectedFoodIndex //vælge fooditem fra listen.
        {
            get { return _selectedFoodIndex; }
            set
            {
                _selectedFoodIndex = value;
                OnPropertyChanged(nameof(SelectedFoodIndex));

            }
        }

        public int SelectedEditFoodIndex //vælge fooditem og edit fra listen.
        {
            get { return _selectedEditFoodIndex; }
            set
            {
                _selectedEditFoodIndex = value;
                OnPropertyChanged(nameof(SelectedEditFoodIndex));
            }

        }
        #endregion

        #region Relaycommands
      
        public Relaycommand CommandPostFood { get; set; }
        public Relaycommand CommandEditFood { get; set; }
        public Relaycommand CommandStartEdit { get; set; }
        public Relaycommand CommandGetFood { get; set; }
        public Relaycommand CommandDelete { get; set; }
        public ObservableCollection<FoodItem> FoodListOB { get; set; }
        public FoodItem Addfood { get; set; }
        public FoodItem FoodToBeEdited;

        #endregion

        #region Const
        /// <summary>
        /// Constructor
        /// </summary>

        public FoodVM() // FoodVM contruktorn.
        {
            FoodListOB = new ObservableCollection<FoodItem>();
            GetFood();
            CommandPostFood = new Relaycommand(PostFood);
            CommandEditFood = new Relaycommand(EditFood);
            CommandStartEdit = new Relaycommand(StartEdit);
            CommandDelete = new Relaycommand(Delete);

            FoodToBeEdited = new FoodItem("", "", "", 0, 0, 0);
            Addfood = new FoodItem("", "", "", 0, 0, 0);
            
        }
        #endregion

        #region Readlist
        /// <summary>
        /// Henter listen af fooditem og gemmer dem i Observable Collection
        /// </summary>
        
        private void GetFood() 
        {
            List<FoodItem> GetFoodItems = FoodPersistencyService.GetFoodItems().Result;
            foreach (FoodItem foods in GetFoodItems)
            {
                FoodListOB.Add(foods);
            }

        }
        #endregion

        #region Add knap
        /// <summary>
        /// For at tilføje til databasen og Observable Collection
        /// </summary>

        public void PostFood() 
        {
            FoodPersistencyService.PostFoodItemAsync(Addfood);
            FoodListOB.Add(Addfood); // tilføjer til OB listen
            OnPropertyChanged(nameof(FoodListOB));
        }

        #endregion

        #region Edit knap
        /// <summary>
        /// For at vælge og edit det du har valgt.
        /// </summary>
        public void StartEdit() 
        {
            if (SelectedFoodIndex != -1)
            {
                FoodToBeEdited = FoodListOB[SelectedFoodIndex];

                EditFoodName = FoodToBeEdited.Name;
                EditBoughFrom = FoodToBeEdited.BoughtFrom;
                EditFkGroupTypeId = FoodToBeEdited.fk_groups_id;
                EditMadeByCompany = FoodToBeEdited.MadeByCompany;
            }
            else
            {
                FeedBack = "Du har ikke Selected noget";
            }

        }
        #endregion

        #region Delete knap
        /// <summary>
        /// for at kunne slette fra databsen og Observable Collection
        /// </summary>
        public void Delete() 
        {
            if (SelectedFoodIndex != -1)
            {

                FoodPersistencyService.DeleteFoodItemAsync(FoodListOB[SelectedFoodIndex]);
                FoodListOB.RemoveAt(SelectedFoodIndex);
                SelectedFoodIndex = -1;
               
            }

        }
        #endregion

        #region Reset af Props
        /// <summary>
        /// For at nulstille sin properties så man kan redigere et nyt element
        /// </summary>
    public void ResetProps() // nulstiller props
        {
            EditFoodName = "";
            EditFkGroupTypeId = 0;
            EditBoughFrom = "";
            EditMadeByCompany = "";

            AddBoughtFrom = "";
            AddFkGroupTypeId = 0;
            AddMadeByCompany = "";
            AddFoodName = "";

            FoodToBeEdited = new FoodItem("", "", "", 0, 0, 0);

        }

        #endregion

        #region Edit/Delete 
        /// <summary>
        /// Fjerner det gamle element og indsætter de redigerede værdier.
        /// </summary>
        public void EditFood() 

        {

            if (FoodToBeEdited.Name != EditFoodName || FoodToBeEdited.BoughtFrom != EditBoughFrom || FoodToBeEdited.MadeByCompany != EditMadeByCompany)
            {
                FoodListOB.Remove(FoodToBeEdited);

                FoodToBeEdited.Name = EditFoodName;
                FoodToBeEdited.BoughtFrom = EditBoughFrom;
                FoodToBeEdited.MadeByCompany = EditMadeByCompany;
                FoodToBeEdited.fk_groups_id = EditFkGroupTypeId;

                MasterPersistencyService<FoodItem>.UpdateAsync(MasterPersistencyService<FoodItem>.DataControllerType.Food,
                    FoodToBeEdited.Id, FoodToBeEdited);

                FoodListOB.Add(FoodToBeEdited);

                ResetProps();

                FeedBack = "It has been Edited";
            }
            else
            {
                FeedBack = "nothing has been changed";
            }
        }
        #endregion

    }
}

