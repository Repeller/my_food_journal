﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyFoodApp.Common;
using MyFoodApp.Model;
using MyFoodApp.Persistency;
using MyFoodApp.View;

namespace MyFoodApp.ViewModel
{
    class LoginVM : INotifyPropertyChanged
    {
        #region PropertyChangeSupport
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion

        public Relaycommand LoginCommand { get; set; }

        public string Username { get; set; }

        private string _password;
        public string Password {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        private string _errorMessage;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        public LoginVM()
        {
            LoginCommand = new Relaycommand(LoginUser);
            ErrorMessage = string.Empty;
        }

        public async void LoginUser()
        {
            // 1. Hent bruger fra databasen
            // 2. Hvis bruger ikke findes eller kodeord ikke matcher vises fejlbesked
            // 3. Bruger logges ind (gemmes i singleton)
            // 4. Redirect til hovedside

            User user = await UserPersistencyService.GetUserAsync(Username);

            if (user == null || user.Password != Password)
            {
                ErrorMessage = "Brugernavn eller kodeord er forkert";
            }

            else
            {
                // Persist user in session
                LoggedInUserSingleton.Instance.LoggedInUser = user;

                // Go to main page
                Frame rootFrame = Window.Current.Content as Frame;
                rootFrame.Navigate(typeof(FrontPage));
            }

            Password = string.Empty;
        }
    }
}
