﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MyFoodApp.Common;
using MyFoodApp.Model;
using MyFoodApp.Persistency;

namespace MyFoodApp.ViewModel
{
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
	class GroupVM : INotifyPropertyChanged
	{
		#region PropertyChangeSupport
		public event PropertyChangedEventHandler PropertyChanged;

		// Create the OnPropertyChanged method to raise the event
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}
		#endregion

		#region fields
		private string _groupId;
		private string _groupName;
		private string _feedback;

		private int _selectedGroupIndex;

		private Group _selectedGroupToBeEdited;

		private string _addView;
		private string _editView;

		private string _editGroupName;
		private int _editGroupId; 
		#endregion

		#region props

		public string GroupId
		{
			get { return _groupId; }
			set
			{
				_groupId = value;
				OnPropertyChanged(nameof(GroupId));
			}
		}
		public string GroupName
		{
			get { return _groupName; }
			set
			{
				_groupName = value;
				OnPropertyChanged(nameof(GroupName));
			}
		}
		public string Feedback
		{
			get { return _feedback; }
			set
			{
				_feedback = value;
				OnPropertyChanged(nameof(Feedback));
			}
		}
		public int EditGroupId
		{
			get
			{
				return _editGroupId;
			}
			set
			{
				_editGroupId = value;
				OnPropertyChanged(nameof(EditGroupId));
			}
		}
		public string EditGroupName
		{
			get
			{
				return _editGroupName;
			}
			set
			{
				_editGroupName = value;
				OnPropertyChanged(nameof(EditGroupName));
			}
		}

		public Group SelectedGroupToBeEdited
		{
			get { return _selectedGroupToBeEdited;}
			set
			{
				_selectedGroupToBeEdited = value;
				OnPropertyChanged(nameof(SelectedGroupToBeEdited));
			}
		}

		public string AddView
		{
			get { return _addView; }
			set
			{
				_addView = value;
				OnPropertyChanged(nameof(AddView));
			}
		}

		public string EditView
		{
			get { return _editView; }
			set
			{
				_editView = value;
				OnPropertyChanged(nameof(EditView));
			}
		}

		public int SelectedGroupIndex
		{
			get
			{
				return _selectedGroupIndex;
			}
			set
			{
				_selectedGroupIndex = value;
				OnPropertyChanged(nameof(SelectedGroupIndex));
			}
		}

		public ObservableCollection<Group> Groups { get; set; }
		#endregion

		#region RelayCommand

		public Relaycommand CommandDelete { get; set; }
		public Relaycommand CommandUpdateList { get; set; }

		public Relaycommand AddStartCommand { get; set; }
		public Relaycommand AddStopCommand { get; set; }
		public Relaycommand AddCommand { get; set; }

		public Relaycommand EditStartCommand { get; set; }
		public Relaycommand EditStopCommand { get; set; }
		public Relaycommand EditGroupCommand { get; set; }

		#endregion

		public GroupVM()
		{
			//CommandAdd = new Relaycommand(CreateGroup);
			
			CommandUpdateList = new Relaycommand(ListUpdate);
			CommandDelete = new Relaycommand(DeleteGroup);

			AddCommand = new Relaycommand(CreateGroup);
			AddStartCommand = new Relaycommand(AddStart);
			AddStopCommand = new Relaycommand(AddStop);

			EditStartCommand = new Relaycommand(EditStartView);
			EditStopCommand = new Relaycommand(EditStop);
			EditGroupCommand = new Relaycommand(EditGroup);

			// TODO : remove this placeholder test
			Groups = new ObservableCollection<Group>();

			// TODO : get data from the database table
			GetData();

			EditView = "Collapsed";
			AddView = "Collapsed";

			// TODO : get date from the user made data table (but only made by this user)
		}

		//TODO : this method "GetData" needs to be reworked. Could be done by the singleTon
		/// <summary>
		/// Gets all the groups from the database and make a OC list of it
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		private void GetData()
		{
			List<Group> tempGroups = new List<Group>();
			tempGroups = GroupPersistencyService.GetGroupsAsync().Result;
			foreach (Group group in tempGroups)
			{
				Groups.Add(group);
			}
		}
		/// <summary>
		/// Clean the OC list and call 'GetData'
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void ListUpdate()
		{
			Groups.Clear();
			GetData();
		}
		/// <summary>
		/// delete the one selected Group from the database and OC list
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void DeleteGroup()
		{
			if (SelectedGroupIndex != -1 && Groups.Count != 0)
			{
				try
				{
					MasterPersistencyService<Group>.DeleteAsync(MasterPersistencyService<Group>.DataControllerType.Groups,
						Groups[SelectedGroupIndex].Id);
					Groups.RemoveAt(SelectedGroupIndex);
					SelectedGroupIndex = -1;
					Feedback = "the group have now been removed";
				}
				catch (Exception e)
				{
					//System.Console.WriteLine(e);
					throw;
				}
			}
			else
			{
				Feedback = "you have not selected an group";
			}
		}


		/// <summary>
		/// resets the 'add' or 'edit' props
		/// </summary>
		/// <param name="isItAdd">true is 'add', false is 'edit'</param>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void ResetProps(bool isItAdd)
		{
			if (isItAdd)
			{
				GroupId = "";
				GroupName = "";
				AddView = "Collapsed";
			}
			else
			{
				EditGroupId = 0;
				EditGroupName = "";
				EditView = "Collapsed";
			}

			Feedback = "";
			SelectedGroupIndex = -1;
		}
		/// <summary>
		/// close edit panel and clean data
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void EditStop()
		{
			ResetProps(false);
		}
		/// <summary>
		/// opens start panel
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void EditStartView()
		{
			if (SelectedGroupIndex != -1 && Groups.Count != 0)
			{
				SelectedGroupToBeEdited = Groups[SelectedGroupIndex];
				EditGroupId = SelectedGroupToBeEdited.Id;
				EditGroupName = SelectedGroupToBeEdited.Name;

				EditView = "Visible";
				Feedback = "";
			}
			else
			{
				Feedback = "du skal vælge noget først";
			}
		}
		/// <summary>
		/// save the new edited group in the database and in your OC list
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void EditGroup()
		{
			// check if the values are new
			// if no, then do the edit
			if (SelectedGroupToBeEdited.Name != EditGroupName)
			{
				Group OldTempGroup = SelectedGroupToBeEdited;

				SelectedGroupToBeEdited.Name = EditGroupName;

				GroupPersistencyService.PutGroupAsync(SelectedGroupToBeEdited);

				Groups[Groups.IndexOf(OldTempGroup)] = SelectedGroupToBeEdited;

				ResetProps(false);

				Feedback = "the edit is now been saved";
			}
			else
			{
				Feedback = "the group name is the same. hvad laver du ?";
			}
		}


		/// <summary>
		/// save the new Group in the database and in your OC list
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void CreateGroup()
		{
			// check if the user did the right thing
			

			int tempId;

			bool IsThereInput;

			if (GroupId != "" && GroupName != "")
			{
				IsThereInput = true;
				Feedback = "";
			}
			else
				IsThereInput = false;


			if (IsThereInput)
			{
				// can the string be converted to an int?
				if (Int32.TryParse(GroupId, out tempId))
				{
                    // add it to the database
					Group tempGroup = new Group(tempId, GroupName);
					MasterPersistencyService<Group>.SaveAsync(MasterPersistencyService<Group>.DataControllerType.Groups,
						tempGroup);

                    // get the newest id from the database
                    List<Group> tempList = MasterPersistencyService<Group>.LoadAsync(
                        MasterPersistencyService<Group>.DataControllerType.Groups).Result;

				    if (tempList.Any())
				    {
				        // find the id and set it on "tempGroup", before you add it to the OC list
				        tempGroup.Id = tempList[(tempList.Count - 1)].Id;

				        // add it to the OC list
				        Groups.Add(tempGroup);

				        ResetProps(true);
				        Feedback = "Den nye mad gruppe er nu oprettet";
                    }
				    else
				    {
				        ResetProps(true);
				        Feedback = "Der skete en (måske async) fejl, tryk på update list knappen.";
                    }
                    
				}
				else
				{
					Feedback = "group id skal være et tal. prøv igen";
				}
			}
			else
			{
				Feedback = "du har ikke udfyldt de 2 textboxs. skriv noget";
			}

		}
		/// <summary>
		/// open the start panel
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void AddStart()
		{
			AddView = "Visible";
			Feedback = "";
		}
		/// <summary>
		/// close the add panel
		/// </summary>
		/// <remarks>
		///	Skrevet af Ziegler
		/// </remarks>
		public void AddStop()
		{
			ResetProps(true);
		}
	}
}
