﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml;
using Windows.Media.Playback;
using MyFoodApp.Common;
using MyFoodApp.Model;
using MyFoodApp.Persistency;
using MyFoodApp.View;
using MyFoodApp.ViewModel;

namespace MyFoodApp.ViewModel
{
    class RawVM : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }



        #region Instance Fields
        private string _RawId;
        private string _feedback;
        private int _selectedRawIndex;
        private int _selectedEditRawIndex;
       
        private string _EditRawName;
        private string _EditMadeByCompany;
        private string _EditBoughtFrom;
        private int _EditGroupTypeId;
        #endregion



        #region Props

        public string FeedBack
        {
            get { return _feedback; }
            set { _feedback = value; OnPropertyChanged(nameof(FeedBack)); }
        }

        public string EditMadeByCompany
        {
            get { return _EditMadeByCompany; }
            set { _EditMadeByCompany = value; OnPropertyChanged(nameof(EditMadeByCompany)); }
        }
        public string EditRawName
        {
            get { return _EditRawName; }
            set { _EditRawName = value; OnPropertyChanged(nameof(EditRawName)); }
        }
        public string EditBoughFrom
        {
            get { return _EditBoughtFrom; }
            set { _EditBoughtFrom = value;OnPropertyChanged(nameof(EditBoughFrom)); }
        }
        public int EditFkGroupTypeId
        {
            get { return _EditGroupTypeId; }
            set { _EditGroupTypeId = value; OnPropertyChanged(nameof(EditFkGroupTypeId)); }
        }

        public int SelectedRawIndex
        {
            get { return _selectedRawIndex; }
            set
            {
                _selectedRawIndex = value;
                OnPropertyChanged(nameof(SelectedRawIndex));

            }
        }
        public int SelectedEditRawIndex
        {
            get { return _selectedEditRawIndex; }
            set { _selectedEditRawIndex = value; OnPropertyChanged(nameof(SelectedEditRawIndex)); }

        }
        public RawItem AddRawItem { get; set; }

        #endregion

        #region RelayCommands
        public Relaycommand CommandAdd { get; set; }
        public Relaycommand CommandDelete { get; set; }
        //public Relaycommand CommandUpdateList { get; set; }
        public Relaycommand CommandStartEdit { get; set; }
        public Relaycommand CommandEditRaw { get; set; }
        public ObservableCollection<RawItem> OBrawListen { get; set; }
        public RawItem AddNewRawItem { get; set; }
        public RawItem RawToBeEdited;
        //public RawItem RawsItem { get; set; } //dette bruger jeg ikke længere pga 
        //har lavet det om og glemte at slette.

        #endregion

        #region Const
        public RawVM()
        {
            CommandAdd = new Relaycommand(AddRaw);
            CommandDelete = new Relaycommand(DeleteRaw);
            CommandEditRaw = new Relaycommand(EditRaw);

            RawToBeEdited = new RawItem("", "", "", 0, 0, 0);
            AddNewRawItem = new RawItem("", "", "",0,0,0);


            OBrawListen = new ObservableCollection<RawItem>();
      
            ApigetRawItems();
            CommandStartEdit = new Relaycommand(StartEdit);
        }
        #endregion


        #region Edit knap
        /// <summary>
        /// tager mine selected værdi og loader det i edit view.
        /// </summary>
        public void StartEdit()
        {
            if (SelectedRawIndex != -1) 
            {
                //eller så længe den ikke er -1, så køre den.
                RawToBeEdited = OBrawListen[SelectedRawIndex];

                EditRawName = RawToBeEdited.Name;
                EditBoughFrom = RawToBeEdited.BoughtFrom;
                EditFkGroupTypeId = RawToBeEdited.fk_grops_id;
                EditMadeByCompany = RawToBeEdited.MadeByCompany;
            }
            else //hvis selected er  = -1, så har jeg ikke valgt noget 
            {
                FeedBack = "Du har ikke Selected noget"; 
            }
        }
        #endregion

        #region Add Knap
        public void AddRaw()
        {
         
            //RawsItem = new RawItem("","","",0,0,0); //dette bruger jeg ikke længere pga 
            //har lavet det om og glemte at slette.
            

            MasterPersistencyService<RawItem>.SaveAsync(
            MasterPersistencyService<RawItem>.DataControllerType.Raw, AddNewRawItem);
            
            OBrawListen.Add(AddNewRawItem);
            //OnPropertyChanged(nameof(OBrawListen)); det her er ikke nødvendig pga
            // obersvable allerede indeholder onpropertychanged.
        }

        #endregion

        #region Delete Knap
        public void DeleteRaw()
        {

            if (SelectedRawIndex == -1) //hvis der ikke noget valgt, så fortæl.
            {
                FeedBack = "you have not selected an Raw item";   
            }

            else //hvis jeg har valgt noget og sletter det, så fortæl
            {
                try
                {
                    RawPersistencyService.DeleteRawItemAsync(OBrawListen[SelectedRawIndex]);
                    OBrawListen.RemoveAt(SelectedRawIndex);
                    SelectedRawIndex = -1;
                    FeedBack = "the Raw item has now been removed";
                }
                catch (Exception e)
                {

                    throw;
                }
            }

          
        }
        #endregion


        #region Reset af Props
        /// <summary>
        /// clear props forbundet til edit.
        /// </summary>
        public void ResetProps() 
        {
            EditRawName = "";
            EditFkGroupTypeId = 0;
            EditBoughFrom = "";
            EditMadeByCompany = "";

            RawToBeEdited = new RawItem("", "", "", 0, 0, 0);

        }
        #endregion

        #region persistency af Edit/Delete
        public void EditRaw()
        {
            //her tjekker jeg om de nye værdier er anderledes end de gamle
            // hvis de er, så gem ændringerne i Databasen eller hvis bare 
            //en er anderledes, så kan den køre igennem.
            if (RawToBeEdited.Name != EditRawName || 
                RawToBeEdited.BoughtFrom != EditBoughFrom || 
                RawToBeEdited.MadeByCompany != EditMadeByCompany)
            {
                OBrawListen.Remove(RawToBeEdited); //sidder i forkert rækkefølge
                //fordi jeg sletter noget jeg ikke ved om det er gået igennem

                RawToBeEdited.Name = EditRawName; //det her skal rykkes med
                RawToBeEdited.BoughtFrom = EditBoughFrom; //det her skal rykkes med
                RawToBeEdited.MadeByCompany = EditMadeByCompany; //det her skal rykkes med
                RawToBeEdited.fk_grops_id = EditFkGroupTypeId; //det her skal rykkes med

                MasterPersistencyService<RawItem>.UpdateAsync(MasterPersistencyService<RawItem>.DataControllerType.Raw,
                RawToBeEdited.Id, RawToBeEdited);
                //OBrawListen.Remove(RawToBeEdited); ligger her.
                OBrawListen.Add(RawToBeEdited);

                ResetProps();

                FeedBack = "It has been Changed";
            }
            else
            {
                FeedBack = "nothing has been changed";
            }
        }
        #endregion
        public async void ApigetRawItems()
        {
            //dette kunne udskiftes med persistency delen istedet for den er "hard coded";

            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response0 = await client.GetAsync("http://localhost:5500/api/Raws");
                if (response0.IsSuccessStatusCode)
                {
                    List<RawItem> Raws = response0.Content.ReadAsAsync<List<RawItem>>().Result;
                    foreach (var rawItem in Raws)
                    {
                        OBrawListen.Add(rawItem);
                    }

                }
                else
                {
                    
                }
            }
        }

    }
}
