﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyFoodApp.Common;
using MyFoodApp.Model;
using MyFoodApp.Persistency;
using MyFoodApp.View;

namespace MyFoodApp.ViewModel
{
    class UpdateProfileVM : INotifyPropertyChanged
    {
        private const int MinimumUsernameLength = 4;
        private const int MinimumPasswordLength = 8;

        private const string ConfirmDeleteProfile = "Venligst bekræft at du ønsker at slette din bruger ved at trykke på 'Slet bruger' igen";

        #region PropertyChangeSupport
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion

        public Relaycommand UpdateProfileCommand { get; set; }
        public Relaycommand DeleteProfileCommand { get; set; }

        private string _username;

        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                OnPropertyChanged(nameof(Username));
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        private string _password2;
        public string Password2
        {
            get { return _password2; }
            set
            {
                _password2 = value;
                OnPropertyChanged(nameof(Password2));
            }
        }

        private string _errorMessage;
        private User _loggedInUser;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        public UpdateProfileVM()
        {
            UpdateProfileCommand = new Relaycommand(UpdateProfile);
            DeleteProfileCommand = new Relaycommand(DeleteProfile);

            _loggedInUser = LoggedInUserSingleton.Instance.LoggedInUser;

            if (_loggedInUser == null)
            {
                Username = string.Empty;
                Password = string.Empty;
                ErrorMessage = "Du skal være logget ind for at kunne ændre din profil";
            }

            else
            {
                Username = _loggedInUser.Username;
                Password = string.Empty;
                ErrorMessage = string.Empty;
            }
        }

        public async void UpdateProfile()
        {
            // 1. Tjek at bruger er logget ind
            // 2. Tjek at brugernavn ikke findes i forvejen
            // 3. Tjek at brugernavn opfylder minimumskrav
            // 4. Tjek at kodeord er udfyldt
            // 5. Tjek at kodeord er ens
            // 6. Tjek at kodeord opfylder minimumskrav
            // 7. Opdater bruger i databasen
            // 8. Redirect til profilside

            if (_loggedInUser == null)
                return;

            User user = null;
            if (Username != _loggedInUser.Username)
            {
                user = await UserPersistencyService.GetUserAsync(Username);
            }

            if (user != null)
            {
                ErrorMessage = "Bruger kan ikke omdøbes. Brugernavn findes allerede";
            }

            else if (Username.Length < MinimumUsernameLength)
            {
                ErrorMessage = $"Brugernavn er for kort. Benyt mindst {MinimumUsernameLength} tegn";
            }

            else if (Password == string.Empty && Password2 == string.Empty)
            {
                ErrorMessage = "Kodeord skal være udfyldt";
            }

            else if (Password != Password2)
            {
                ErrorMessage = "Kodeord skal være ens";
            }

            else if (Password.Length < MinimumPasswordLength)
            {
                ErrorMessage = $"Password er for kort. Benyt mindst {MinimumPasswordLength} tegn";
            }

            else
            {
                _loggedInUser.Username = Username;
                _loggedInUser.Password = Password;

                // Update user in DB
                UserPersistencyService.PutUserAsync(_loggedInUser);

                // Go to main page
                Frame rootFrame = Window.Current.Content as Frame;
                rootFrame.Navigate(typeof(FrontPage));
            }

            Password = string.Empty;
            Password2 = string.Empty;
        }

        public void DeleteProfile()
        {
            if (_loggedInUser == null)
                return;

            if (ErrorMessage != ConfirmDeleteProfile)
            {
                ErrorMessage = ConfirmDeleteProfile;
            }

            else
            {
                // Delete user in DB
                UserPersistencyService.DeleteUserAsync(_loggedInUser);

                // Logout user
                LoggedInUserSingleton.Instance.LoggedInUser = null;

                // Go to main page
                Frame rootFrame = Window.Current.Content as Frame;
                rootFrame.Navigate(typeof(FrontPage));
            }
        }
    }
}
