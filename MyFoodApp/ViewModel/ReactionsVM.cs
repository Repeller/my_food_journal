﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MyFoodApp.Common;
using MyFoodApp.Model;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography.X509Certificates;
using Windows.UI.Xaml.Controls;
using MyFoodApp.Persistency;

namespace MyFoodApp.ViewModel
{
    class ReactionsVM : INotifyPropertyChanged ///Reaction viewmodel klassen arver fra INotifyPropertyChanged
                                              
    {
        #region PropertyChangeSupport
            public event PropertyChangedEventHandler PropertyChanged;

            // Create the OnPropertyChanged method to raise the event
            protected void OnPropertyChanged(string name)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                { handler(this, new PropertyChangedEventArgs(name));
                }
            }
        #endregion

        #region Fields
        /// <summary>
        /// Instans felter
        /// </summary>
        private int _editedtype_fk_id;
        private int _editedrating;
        private string _editedcomment;
        private int _editedfk_food_id;
        private int _selectedreaction;
        private int _selectedreactionindex;
        private int _reactid;
        private string _errormsg;
        #endregion

        #region Properties

        public Relaycommand CommandGetReact { get; set; }
        public Relaycommand CommandPostReact { get; set; }
        public Relaycommand CommandDeleteReact { get; set; }
        public Relaycommand CommandEdit { get; set; }
        public Relaycommand CommandRedigerValgt { get; set; }

        public ObservableCollection<Reactions> ReactionlistOB { get; set; }
        public Reactions ReactionToBeEdited;

        public int Id { get; set; }
        public int FK_type { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
        public int Fk_food { get; set; }
        public Reactions Addreaction { get; set; }

        public int ReactID
        {
            get { return _reactid; }
            set
            {
                _reactid = value;
                OnPropertyChanged(nameof(ReactID));
            }
        }
        public int SelectedReact
        {
            get { return _selectedreaction; }
            set   
            {
                _selectedreaction = value;
                OnPropertyChanged(nameof(SelectedReact));
            }
        }
        public int Selectedreactionindex
        {
            get { return _selectedreactionindex; }
            set { _selectedreactionindex = value; OnPropertyChanged(nameof(Selectedreactionindex)); }
        }
        public string Errormsg
        {
            get { return _errormsg;}
            set { _errormsg = value; OnPropertyChanged(nameof(Errormsg)); }
        }
        public int Editedtype_fk_id
        {
            get { return _editedtype_fk_id; }
            set { _editedtype_fk_id = value; OnPropertyChanged(nameof(Editedtype_fk_id)); }
        }
        public int EditedRating
        {
            get { return _editedrating; }
            set { _editedrating = value; OnPropertyChanged(nameof(EditedRating)); }
        }
        public string EditedComment
        {
            get { return _editedcomment; }
            set { _editedcomment = value; OnPropertyChanged(nameof(EditedComment)); }
        }
        public int Editedfk_food_id
        {
            get { return _editedfk_food_id; }
            set { _editedfk_food_id = value; OnPropertyChanged(nameof(Editedfk_food_id)); }
        }
        #endregion

        #region Construtor
        /// <summary>
        /// Constructor
        /// </summary>
        public ReactionsVM()
        {
            CommandGetReact = new Relaycommand(GetReactions);
            CommandPostReact = new Relaycommand(PostReaction);
            CommandDeleteReact = new Relaycommand(DeleteReaction);
            CommandEdit = new Relaycommand(RedigerReaction);
            CommandRedigerValgt = new Relaycommand(Editedvalues); 
            Addreaction = new Reactions(0,0,0,"",0);
            ReactionToBeEdited = new Reactions(0, 0, 0, "", 0);
            ReactionlistOB = new ObservableCollection<Reactions>();

            GetReactions();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Henter listen af Reaktioner og gemmer dem i Observable Collection
        /// </summary>
        private void GetReactions()
        {
            List<Reactions> Reactionslist = ReactionPersistencyService.GetReactions();
            foreach (Reactions reactions in Reactionslist)
            {
                ReactionlistOB.Add(reactions);
            }
        }
        /// <summary>
        /// Tilføjer værdier til et tomt objekt og gemmer det i Observable Collection
        /// </summary>
        public void PostReaction()
        {
            ReactionPersistencyService.PostReactionAsync(Addreaction);
            ReactionlistOB.Add(Addreaction);
            OnPropertyChanged(nameof(ReactionlistOB));
        }

        //TODO : mangler kodekommentering
        public void Editedvalues()
        {
            if (SelectedReact != -1)
            {
                ReactionToBeEdited = ReactionlistOB[SelectedReact];

                Editedfk_food_id = ReactionToBeEdited.Fk_food_id;
                EditedComment = ReactionToBeEdited.Comment;
                EditedRating = ReactionToBeEdited.Rating;
                Editedtype_fk_id = ReactionToBeEdited.Fk_type_id;
            }
            else
            {
                Errormsg = "Det valgte element kan ikke redigeres";
            }

        }
        /// <summary>
        /// Opdater det valgte element med nye værdier
        /// </summary>
        public void RedigerReaction()
        {
            if (ReactionToBeEdited.Rating != EditedRating || ReactionToBeEdited.Comment != EditedComment || ReactionToBeEdited.Fk_food_id != Editedfk_food_id || ReactionToBeEdited.Fk_type_id != Editedtype_fk_id)
            {
                ReactionlistOB.Remove(ReactionToBeEdited);

                ReactionToBeEdited.Rating = EditedRating;
                ReactionToBeEdited.Comment = EditedComment;
                ReactionToBeEdited.Fk_food_id = Editedfk_food_id;
                ReactionToBeEdited.Fk_type_id = Editedtype_fk_id;

                MasterPersistencyService<Reactions>.UpdateAsync(MasterPersistencyService<Reactions>.DataControllerType.FoodReaction,
                    ReactionToBeEdited.Id, ReactionToBeEdited);

                ReactionlistOB.Add(ReactionToBeEdited);

                ResetProps();
            }
            else
            {
                Errormsg = "Det vaglte element kan ikke redigeres";
            }
        }
        /// <summary>
        /// Nulstiller properties så man kan redigere et nyt element
        /// </summary>
        public void ResetProps()
        {
            Editedfk_food_id = 0;
            EditedComment = "";
            EditedRating = 0;
            Editedtype_fk_id = 0;

            FK_type = 0;
            Comment = "";
            Rating = 0;
            Fk_food = 0;

            ReactionToBeEdited = new Reactions(0, 0, 0, "", 0);
        }
        /// <summary>
        /// Fjerner element fra listen
        /// </summary>
        public void DeleteReaction()
        {
            if (SelectedReact != -1)
            {
                ReactionPersistencyService.DeleteGroupAsync(ReactionlistOB[SelectedReact]);
                ReactionlistOB.RemoveAt(SelectedReact);
                SelectedReact = -1;
            }
        }
        #endregion
    }
}
