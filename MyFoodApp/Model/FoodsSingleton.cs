﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp.Model
{
    /// <summary>
    /// The singleton that holds all the food-data, that the user have created
    /// </summary>
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
    class FoodsSingleton
    {
        private static FoodsSingleton _instance = new FoodsSingleton();

        public static FoodsSingleton Instance
        {
            get { return _instance ?? (_instance = new FoodsSingleton()); }
        }

        /// <summary>
        /// the list of 'foods', that have all the data connected to one food entity in one object
        /// </summary>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static ObservableCollection<FoodClass> Foods { get; set; }

        private FoodsSingleton()
        {
            Foods = new ObservableCollection<FoodClass>();
        }

        public static void GetTheData(int UserId)
        {
            // TODO : talk with all the other singletons, that get the data from the database
            // maybe use a custom-query on the API 'get' method
        }
    }

    /// <summary>
    /// All the data connected to one 'FoodItem', stored in one single object
    /// </summary>
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
    class FoodClass
    {
        public FoodItem Item { get; set; }
        public bool Organic { get; set; }
        public ObservableCollection<Reactions> AllTheFoodReactions { get; set; }

        public FoodClass()
        {

        }

        /// <summary>
        /// Insert all the data, that is connected to just one FoodItem (in the database)
        /// </summary>
        /// <param name="food">the FoodItem entity</param>
        /// <param name="organic">is it organic? yes=true, no=false</param>
        /// <param name="allTheFoodReactions">all the food reactions that is connected to this foodItem</param>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public FoodClass(FoodItem food, bool organic, List<Reactions> allTheFoodReactions)
        {
            Item = food;
            Organic = organic;
            foreach (Reactions reaction in allTheFoodReactions)
            {
                AllTheFoodReactions.Add(reaction);
            }
        }

        public override string ToString()
        {
            return $"{nameof(Item)}: {Item}, {nameof(Organic)}: {Organic}, {nameof(AllTheFoodReactions)}: {AllTheFoodReactions}";
        }
    }
}