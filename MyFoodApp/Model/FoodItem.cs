﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp
{
    class FoodItem
    {

        public string Name { get; set; }
        public string BoughtFrom { get; set; }
        public string MadeByCompany { get; set; }

        public int Id { get; set; }
        public int fk_user_id { get; set; }
        public int fk_groups_id { get; set; }

        public FoodItem(string name, string boughtFrom, string madeByCompany, 
                         int foodId, int fk_User, int fk_Group)
        {
            Name = name;
            BoughtFrom = boughtFrom;
            MadeByCompany = madeByCompany;
            Id = foodId;
            fk_user_id = fk_User;
            fk_groups_id = fk_Group;
        }


        public override string ToString()
        {
            return $" {nameof(Name)}: {Name}, {nameof(BoughtFrom)}: {BoughtFrom}," +
            $" {nameof(MadeByCompany)}: {MadeByCompany}, {nameof(fk_groups_id)}: {fk_groups_id}";
        }
    }
}
