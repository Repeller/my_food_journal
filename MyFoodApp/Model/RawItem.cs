﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp.Model
{
    class RawItem
    {
        //public string _name;
        //public string _boughtFrom;
        //public string _madeByCompany;


        public string Name { get; set; }
        public string BoughtFrom { get; set; }
        public string MadeByCompany { get; set; }

        public int Id { get; set; }

        public int fk_grops_id { get; set; }

        public int fk_user_id { get; set; }


        public RawItem(string name, string boughtFrom, string madeByCompany, int id, int Fk_grops_id, int Fk_user_id)
        {
            Name = name;
            BoughtFrom = boughtFrom;
            MadeByCompany = madeByCompany;
            Id = id;
            fk_grops_id = Fk_grops_id;
            fk_user_id = Fk_user_id;
        }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(BoughtFrom)}: {BoughtFrom}, {nameof(MadeByCompany)}: {MadeByCompany}, {nameof(Id)}: {Id}, {nameof(fk_grops_id)}: {fk_grops_id}, {nameof(fk_user_id)}: {fk_user_id}";
        }
    }
}
