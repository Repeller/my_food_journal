﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp.Model
{
    class Reactions
    {
        public int Id { get; set; }
        public int Fk_type_id { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
        public int Fk_food_id { get; set; }

        public Reactions(int reactId, int fk_type, int rating, string comment, int fk_food)
        {
            Id = reactId;
            Rating = rating;
            Comment = comment;
            Fk_food_id = fk_food;
            Fk_type_id = fk_type;
        }
        public override string ToString()
        {
            return $"{nameof(Fk_type_id)}: {Fk_type_id}, {nameof(Rating)}: {Rating}, {nameof(Comment)}: {Comment}, {nameof(Fk_food_id)}: {Fk_food_id}";
        }
    }
}
