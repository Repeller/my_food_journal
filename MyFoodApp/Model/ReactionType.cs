﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp.Model
{
    /// <summary>
    /// the 'ReactionType' class that holds the data from the database
    /// </summary>
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
	class ReactionType
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int fk_user_id { get; set; }

		public ReactionType(int id, string name, int fkUserId)
		{
			Id = id;
			Name = name;
			fk_user_id = fkUserId;
		}

		public override string ToString()
		{
			return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(fk_user_id)}: {fk_user_id}";
		}
	}
}
