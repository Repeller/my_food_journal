﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp.Model
{
    class LoggedInUserSingleton
    {
        // Using the thread-safe Lazy<T> class ensures that the LoggedInUserSingleton is only initialized when requested for the first time
        private static readonly Lazy<LoggedInUserSingleton> Lazy = new Lazy<LoggedInUserSingleton>(() => new LoggedInUserSingleton());

        public static LoggedInUserSingleton Instance { get { return Lazy.Value; } }


        // The logged-in user contained within the singleton
        public User LoggedInUser { get; set; }

        // Private constructor to prevent other classes from initializing the singleton
        private LoggedInUserSingleton()
        {
        }
    }
}
