﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp.Model
{
    /// <summary>
    /// The singleton that holds all the raw-data, that the user have created
    /// </summary>
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
    class RawsSingleton
    {
        private static RawsSingleton _instance = new RawsSingleton();

        public static RawsSingleton Instance
        {
            get
            {
                return _instance ?? (_instance = new RawsSingleton());
            }
        }

        /// <summary>
        /// the list of 'raws', that have all the data connected to one raw entity in one object
        /// </summary>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public static ObservableCollection<RawClass> Raws { get; set; }

        private RawsSingleton()
        {
            Raws = new ObservableCollection<RawClass>();
        }

        public static void GetTheData(int UserId)
        {
            // TODO : talk with all the other singletons, that get the data from the database
            // maybe use a custom-query on the API 'get' method
        }
    }

    /// <summary>
    /// All the data connected to one 'RawItem', stored in one single object
    /// </summary>
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
    class RawClass
    {
        public RawItem Item { get; set; }
        public bool Organic { get; set; }
        public ObservableCollection<Reactions> AllTheRawReactions { get; set; }

        public RawClass()
        {
            
        }

        /// <summary>
        /// Insert all the data, that is connected to just one RawItem (in the database)
        /// </summary>
        /// <param name="raw">the RawItem entity</param>
        /// <param name="organic">is it organic? yes=true, no=false</param>
        /// <param name="allTheRawReactions">all the raw reactions that is connected to this RawItem</param>
        /// <remarks>
        /// Skrevet af Ziegler
        /// </remarks>
        public RawClass(RawItem raw, bool organic, List<Reactions> allTheRawReactions)
        {
            Item = raw;
            Organic = organic;
            foreach (Reactions reaction in allTheRawReactions)
            {
                AllTheRawReactions.Add(reaction);
            }
        }

        public override string ToString()
        {
            return $"{nameof(Item)}: {Item}, {nameof(Organic)}: {Organic}, {nameof(AllTheRawReactions)}: {AllTheRawReactions}";
        }
    }
}
