﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp.Model
{
    /// <summary>
    /// The 'group' class that holds the data from the database
    ///  </summary>
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
	class Group
	{
		public int Id { get; set; }
		public string Name { get; set; }


		public Group(int id, string name)
		{
			Id = id;
			Name = name;
		}
		public override string ToString()
		{
			return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}";
		}
	}
}
