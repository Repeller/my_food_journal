﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp.Model
{
    /// <summary>
    /// the 'userMadeGroup' that holds the data from the datbase
    /// </summary>
    /// <remarks>
    /// Skrevet af Ziegler
    /// </remarks>
	class UserMadeGroup : Group
	{
		public int fk_user_id { get; set; }

		public UserMadeGroup(int id, string name, int fkUserId) : base (id, name)
		{
			fk_user_id = fkUserId;
		}

		public override string ToString()
		{
			return $"{base.ToString()}, {nameof(fk_user_id)}: {fk_user_id}";
		}
	}
}
