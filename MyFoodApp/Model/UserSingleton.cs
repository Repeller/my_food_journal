﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFoodApp.Model
{
    class UserSingleton
    {
        private static UserSingleton _instance;

        public static UserSingleton Instance
        {
            get
            {
                return _instance ?? (_instance = new UserSingleton());
            }
        }

        public User LoginUser { get; set; }
    }
}
