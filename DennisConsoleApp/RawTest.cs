﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DennisConsoleApp
{
    class RawTest
    {
        public string _name { get; set; }
        public string _boughtFrom { get; set; }
        public string _madeByCompany { get; set; }

        public RawTest(string name, string boughtFrom, string madeByCompany)
        {
            _name = name;
            _boughtFrom = boughtFrom;
            _madeByCompany = madeByCompany;
        }

        public override string ToString()
        {
            return $"{nameof(_name)}: {_name}, {nameof(_boughtFrom)}: {_boughtFrom}, {nameof(_madeByCompany)}: {_madeByCompany}";
        }
    }
}
