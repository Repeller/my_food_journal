﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DennisConsoleApp
{
    class FoodTest
    {
        public string Name { get; set; }
        public string BoughtFrom { get; set; }
        public string MadeByCompany { get; set; }

        public FoodTest(string name, string boughtFrom, string madeByCompany)
        {
            Name = name;
            BoughtFrom = boughtFrom;
            MadeByCompany = madeByCompany;
        }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(BoughtFrom)}: {BoughtFrom}, {nameof(MadeByCompany)}: {MadeByCompany}";
        }
    }
}
