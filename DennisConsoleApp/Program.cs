﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WebAPI2;

namespace DennisConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                  
                Console.WriteLine("http://localhost:5500/api/Foods");
                var response1 = client.GetAsync("http://localhost:5500/api/Foods").Result;
                if (response1.IsSuccessStatusCode)
                {
                    IEnumerable<FoodTest> FoodList = response1.Content.ReadAsAsync<IEnumerable<FoodTest>>().Result;
                    foreach (var FoodTest in FoodList)

                    {
                        Console.WriteLine(FoodTest);
                    }
                }
                async void PostFoodItemAsync(FoodTest foodItem)
                 {
                     var post = await client.PostAsJsonAsync("http://localhost:5500/api/Foods", foodItem);
                 }

                PostFoodItemAsync(new FoodTest("Kebab", "Kebab100", "Kød coop"));

                Console.ReadKey();
               


            }
        }
    }
}
