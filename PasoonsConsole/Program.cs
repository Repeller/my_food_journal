﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WebAPI2;
using WebAPI2.Models;

namespace PasoonsConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            const string ServerUrl = "http://localhost:5500";
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(ServerUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                
                // print get 
                var response1 = client.GetAsync("http://localhost:5500/api/Raws").Result;
                if (response1.IsSuccessStatusCode)
                {
                    IEnumerable<WebAPI2.Models.Raw> RawList = response1.Content.ReadAsAsync<IEnumerable<WebAPI2.Models.Raw>>().Result;
                    foreach (var RawItem in RawList)
                    {
                        Console.WriteLine(RawItem);
                    }
                }

                WebAPI2.Models.Raw testRawItem = new WebAPI2.Models.Raw()
                {
                    BoughtFrom = "test1 eaew", fk_grops_id = 4523, Id = 52, fk_user_id = 222, MadeByCompany = "ik2ea",
                    Name = "2what  ever"
                };
                
               
                // ADD NEW
                var response23 = client.PostAsJsonAsync(ServerUrl + "/api/raws", testRawItem).Result;

                Console.WriteLine("you have now added a new item");

                // get / read / print
                response1 = client.GetAsync("http://localhost:5500/api/Raws").Result;
                if (response1.IsSuccessStatusCode)
                {
                    IEnumerable<WebAPI2.Models.Raw> RawList = response1.Content.ReadAsAsync<IEnumerable<WebAPI2.Models.Raw>>().Result;
                    foreach (var RawItem in RawList)
                    {
                        Console.WriteLine(RawItem);
                    }
                }



                //async void PutRawItemAsync(FoodAPI.Models.Raw RawItem)
                //{
                //    var put = await client.PutAsJsonAsync("http://localhost:5500/api/Raws", RawItem);
                //}

                //async void DeleteRawItemAsync()
                //{
                //    var delete = await client.DeleteAsync("http://localhost:5500/api/Raws");
                //}

                //async void PostRawItemAsync(FoodAPI.Models.Raw RawItem)
                //{
                //    var post = await client.PostAsJsonAsync("http://localhost:5500/api/Raws", RawItem);
                //}

                Console.ReadKey();


                // PutFoodItemAsync();
                //DeleteRawItemAsync();

            }

        }
    }
}
