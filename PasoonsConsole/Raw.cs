﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PasoonsConsole
{
    class Raw
    {
        public ObservableCollection<Raw> Raw2 { get; set; }
        
        //public string _name;
        //public string _boughtFrom;
        //public string _madeByComapy;

        public string Name { get; set; }
        public string Boughtfrom { get; set; }
        public string Madebycomapy { get; set; }

        public Raw(string name, string boughtfrom, string madebycomapy)
        {
            Name = name;
            Boughtfrom = boughtfrom;
            Madebycomapy = madebycomapy;
            Raw2 = new ObservableCollection<Raw>();
        }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Boughtfrom)}: {Boughtfrom}, {nameof(Madebycomapy)}: {Madebycomapy}";
        }
    }
}
